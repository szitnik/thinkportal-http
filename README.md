## Think!Portal Http

HTTP library for Think!Portal. It provides the necessary tools for connecting 
to any RESTful API backend.
