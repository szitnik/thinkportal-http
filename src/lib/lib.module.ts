import {NgModule, Optional, SkipSelf} from '@angular/core';
import {SharedModule} from 'thinkportal-core';
import {DemographicsPartyService} from './services/demographics-party/demographics-party.service';
import {RelationService} from './services/relation/relation.service';
import {ActionEventService} from './services/action-event/action-event.service';
import {EhrViewService} from './services/ehr-view/ehr-view.service';
import {HttpService} from './services/http/http.service';

@NgModule({
  imports: [SharedModule],
  providers: [
    DemographicsPartyService,
    RelationService,
    ActionEventService,
    EhrViewService,
    HttpService
  ]
})
export class ThinkPortalHttpModule {
  constructor (@Optional() @SkipSelf() parentModule: ThinkPortalHttpModule) {
    if (parentModule) {
      throw new Error('ThinkPortalHttpModule is already loaded. Import it in the AppModule only.');
    }
  }
  static reducers() {
    return {};
  }
}
