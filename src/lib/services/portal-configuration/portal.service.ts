/**
 * Purpose: management of portal configurations
 *
 * URL: http://thinkehr4.marand.si:8087/rest/v1/...
 * Docs: http://thinkehr4.marand.si:8087/swagger-ui.html
 */
import {Injectable} from '@angular/core';
import {HttpService} from '../http/http.service';

@Injectable()
export class PortalService{

    constructor(public httpService:HttpService) {}

}