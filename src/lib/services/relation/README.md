## Relations API

Purpose: access to relations between users and patient EHRs.

URL: http://thinkehr4.marand.si:8085/rest/v1/relation?name={relation_name}
Docs: TBD

relation_name parameter can have the following values:
- OWNS: is a relation between a user and its own EHR
- PERSON_OF_TRUST: is a relation between a user and EHRs she is allowed to manage


The call returns an array of EHR ids, i.e.:
```json
[
    "56fc21b9-1099-4845-b9ad-93b65b643014"
]
```