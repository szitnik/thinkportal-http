import {Injectable} from '@angular/core';
import {Observable} from "rxjs/Rx";
import {Http, Request, RequestMethod, Response} from '@angular/http';
import {RequestArgs} from '@angular/http/src/interfaces';
import {AuthService} from "thinkportal-core";
import {IHttpInterceptors} from "../../interfaces/http.interface";
import {ModalService} from 'thinkportal-core';

@Injectable()
export class HttpService {

    private interceptors: IHttpInterceptors = {
        requestOptionsArgs: [],
        afterRequest: {}
    };

    constructor(private http: Http, private authService: AuthService, private modalService: ModalService) {}

    public REQUEST(request: Request): Observable<any> {
        return this.http
            .request(request)
            .flatMap((response: any) => this.handleResponse(request, response))
            .catch((response: any) => this.handleError(request, response));
    }

    public GET(url: string): Observable<any> {
        return this.REQUEST(this.getRequest(url, RequestMethod.Get));
    }

    public POST(url: string, data: any): Observable<any>  {
        return this.REQUEST(this.getRequest(url, RequestMethod.Post, JSON.stringify(data)));
    }

    public PUT(url: string, data: any): Observable<any> {
        return this.REQUEST(this.getRequest(url, RequestMethod.Put, JSON.stringify(data)));
    }

    public DELETE(url: string): Observable<any> {
        return this.REQUEST(this.getRequest(url, RequestMethod.Delete));
    }

    private showAlert(msg: string) {
        this.modalService.open({
            title: 'Internet connection problem',
            message: msg,
            type: ModalService.ALERT
        });
    }

    private handleError(request: Request, response: any|Response): Observable<any> {
        let stream: Observable<any>;
        stream = this.afterRequestInterceptors(request, response);
        if (stream === undefined) {
            let errMsg = (response.message) ?
                response.message : response.status ?
                `${response.status} - ${response.statusText}` : 'An error occurred during HTTP request';
            stream = Observable.throw(errMsg);
            this.showAlert(errMsg);
        } else {
            stream.catch(() => {
                let errMsg = 'An error occurred during HTTP request';
                this.showAlert(errMsg);
                return Observable.throw(errMsg);
            });
        }
        return stream;
    }

    private handleResponse(request: Request, response: Response): Observable<any> {
        let stream: Observable<any>;
        stream = this.afterRequestInterceptors(request, response);
        if (stream === undefined) {
            stream = Observable.create((observer) => {
                if (response && response.status == 200) {
                    observer.next(response.json());
                } else {
                    observer.next(null);
                }
            });
        }
        return stream;
    }

    private getRequest(url: string, method: RequestMethod, body?: any): Request {
        let requestArgs = <RequestArgs>{
            url : url,
            method : method
        };
        if (body !== undefined) {
            requestArgs.body = body;
        }
        requestArgs = this.getRequestOptionsArgs(requestArgs);
        return new Request(requestArgs);
    }

    private getRequestOptionsArgs(requestArgs: RequestArgs): RequestArgs {
        for (let requestOptionsArgsCallback of <Function[]>this.interceptors.requestOptionsArgs) {
            requestArgs = requestOptionsArgsCallback(requestArgs, this.authService);
        }
        return requestArgs;
    }

    private afterRequestInterceptors(request: Request, response: Response): Observable<any> {
        let stream: Observable<any>;

        if (response) {
            if (response.status !== undefined) {
                if (this.interceptors.afterRequest[response.status] !== undefined) {
                    for (let afterRequestCallback of <Function[]>this.interceptors.afterRequest[response.status]) {
                        if (stream === undefined) {
                            stream = afterRequestCallback(request, response, this);
                        } else {
                            stream = stream
                                .flatMap((newResponse) => {
                                    return afterRequestCallback(request, newResponse, this);
                                });
                        }
                    }
                }
            }
        }

        return stream;
    }

    public registerInterceptors(interceptors: IHttpInterceptors): void {
        if (interceptors.requestOptionsArgs !== undefined) {
            (<Function[]>this.interceptors.requestOptionsArgs).push(<Function>interceptors.requestOptionsArgs);
        }
        if (interceptors.afterRequest !== undefined) {
            for(let afterRequest in interceptors.afterRequest) {
                if (interceptors.afterRequest.hasOwnProperty(afterRequest)) {
                    if (this.interceptors.afterRequest[afterRequest] === undefined) {
                        this.interceptors.afterRequest[afterRequest] = [];
                    }
                    (<Function[]>this.interceptors.afterRequest[afterRequest]).push(<Function>interceptors.afterRequest[afterRequest]);
                }
            }
        }
    }
}
