## ActionEvents API

Purpose: management of events (reminders, tasks, etc.)

URL: http://thinkehr4.marand.si:8088/rest/v1/...
Docs: http://thinkehr4.marand.si:8088/swagger-ui.html
