/**
 * Purpose: management of events (reminders, tasks, etc.)
 *
 * URL: http://thinkehr4.marand.si:8088/rest/v1/...
 * Docs: http://thinkehr4.marand.si:8088/swagger-ui.html
 */
import {Injectable} from '@angular/core';
import {Response} from '@angular/http';
import {HttpService} from '../http/http.service';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class ActionEventService {

    private baseUrl: string = 'http://thinkehr4.marand.si:8088';

    constructor(public httpService:HttpService) {}

    /**
     * Get events for user
     */
    getEvents(ehrId:string): Observable<Response> {
        return this.httpService
            .GET(this.baseUrl + '/rest/v1/event?to=' + ehrId)
            .map((actionEvents:any) => {
                return (<any>actionEvents).content;
            });
    }

}