import * as moment from 'moment';

export class DemographicsParty {

	public id: string;
	public ehrId: string;
	public name: string;
	public dob: moment.Moment;
	public gender: string;
	public age: any;
	public picture: string;
	public address: string;

	constructor(data: any, ehrId?) {
		if (data !== null) {
			this.id = data.id;
			this.name = data.firstNames + ' ' + data.lastNames;
			this.dob = moment(data.dateOfBirth);

			let today = moment().add(1, 'days');
			let ageYears = today.diff(this.dob, 'years');
			let dobYears = moment(this.dob).add(ageYears, 'years');
			let ageMonths = today.diff(dobYears, 'months');
			let dobMonths = moment(dobYears).add(ageMonths, 'months');
			let ageDays = today.diff(dobMonths, 'days');
			this.age = {
				years : ageYears,
				months : ageMonths,
				days : ageDays
			};
			this.gender = data.gender;
			this.address = '-';

			let imageUrl;
			if (data.hasOwnProperty('partyAdditionalInfo')) {
				data.partyAdditionalInfo.forEach(function (el) {
					if (el.key === 'image_url') {
						imageUrl = el.value;
					} else if (el.key === 'ehrId') {
						this.ehrId = el.value;
					}
				});
			}

			if (imageUrl !== undefined) {
				this.picture = imageUrl;
			} else {
				this.picture = this.partyPicture();
			}

			if (this.ehrId === undefined) {
				this.ehrId = ehrId;
			}
		}
	}

	private partyPicture() {
		let picYears = [1, 3, 6, 11, 18, 25, 40, 50, 60, 70, 80];

		let pic = 'patient-';
		if (this.age.years == 0) {
			if (this.age.months == 0) {
				pic += '00'
			} else if (this.age.months == 1) {
				pic += '01m';
			} else {
				pic += '01y';
			}
		} else {
			let picY = 0;
			for (let i = 0; i < picYears.length; i++) {
				if (this.age.years >= picYears[i]) {
					if (i == (picYears.length - 1)) {
						picY = picYears[i];
						break;
					} else {
						if (this.age.years < picYears[i+1]) {
							if (this.age.years < (picYears[i+1]/2)) {
								picY = picYears[i];
							} else {
								picY = picYears[i+1];
							}
							break;
						}
					}
				}
			}
			if (picY < 10) {
				pic += '0' + picY + 'y';
			} else {
				pic += picY + 'y';
			}
		}

		if (this.gender == 'FEMALE') {
			pic += '-f';
		} else {
			pic += '-m';
		}

		pic += '.png';

		return 'assets/thinkportal-phr/img/patients/' + pic;
	}

}