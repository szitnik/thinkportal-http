import {DemographicsParty} from './demographics-party';
import {HttpService} from "../http/http.service";
import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';

@Injectable()
export class DemographicsPartyService {

	constructor (private httpService: HttpService) {}

	get(ehrId:string): Observable<DemographicsParty> {
		return this.httpService
			.GET('http://thinkehr4.marand.si:38081/rest/v1/demographics/ehr/' + ehrId + '/party')
			.map((data:any) => new DemographicsParty(data.party, ehrId));
	}

	// Currently not availables
	/*search(name): Observable<DemographicsParty[]> {
		let data:any = [ { key : "search", value : '*' + name + '*' } ];
		return this.httpService
			.POST('http://thinkehr4.marand.si:38081/rest/v1/demographics/party/query', data)
			.map((data:any) => {
				let parties:DemographicsParty[] = [];
				if (data) {
					for (let i = 0; i < data.parties.length; i++) {
						parties.push(new DemographicsParty(data.parties[i]));
					}
				}
				return parties;
			});
	}*/

}