## EHR VIEW REST APIs

Purpose: access to healthcare and demographic data.

URL: http://thinkehr4.marand.si:38081/rest/v1/... 
Docs: 
- http://thinkehr4.marand.si:38081/rest-doc
- https://dev.ehrscape.com/api-explorer.html 