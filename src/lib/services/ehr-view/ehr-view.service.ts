/**
 * Purpose: access to healthcare and demographic data.
 *
 * URL: http://thinkehr4.marand.si:38081/rest/v1/... 
 * Docs: 
 * - http://thinkehr4.marand.si:38081/rest-doc
 * - https://dev.ehrscape.com/api-explorer.html 
 */
import {Injectable} from '@angular/core';
import {HttpService} from '../http/http.service';
import {Observable} from 'rxjs/Observable';
import {EhrView} from './ehr-view';

@Injectable()
export class EhrViewService {

    private baseUrl = 'http://thinkehr4.marand.si:38081';

    constructor(public httpService:HttpService) {}

    get(ehrId, view): Observable<EhrView> {
        return this.httpService
            .GET(this.baseUrl + '/rest/v1/view/' + ehrId + '/' + view)
            .map((data:any) => new EhrView(data));
    }

}