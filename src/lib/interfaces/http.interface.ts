export interface IHttpInterceptors {
    requestOptionsArgs?: Function|Function[];
    afterRequest?: {[httpCode: number] : Function|Function[]};
}